#pragma once
typedef unsigned char uchar ;
#include <string>
using namespace std ;
class Chien {
    private:
        string nom ;
        uchar age ;
    
    public:
        Chien(string prmNom = "Snoopy");
        ~Chien();
        string LireNom() ;
        void EcrireNom(string prmNom) ;
        uchar LireAge(void) ;
        void EcrireAge(uchar prmAge) ;
        
};